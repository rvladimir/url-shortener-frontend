const eslintPluginPrettier = require("eslint-plugin-prettier");
module.exports = {
  root: true,
  env: {browser: true, es2020: true},
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parser: '@typescript-eslint/parser',
  plugins: ['react-refresh', 'prettier'],
  rules: {
    'react-refresh/only-export-components': [
      'warn',
      {allowConstantExport: true},
    ],
    indent: ['error', 2],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    "no-console": ['error'],
    "react-hooks/exhaustive-deps": "off"
  },
}
