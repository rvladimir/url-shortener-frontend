import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@services': `${path.resolve(__dirname, './src/services/')}`,
      '@store': `${path.resolve(__dirname, './src/store/')}`,
      '@components': `${path.resolve(__dirname, './src/components/')}`,
      '@ts': `${path.resolve(__dirname, './src/ts/')}`,
      '@pages': `${path.resolve(__dirname, './src/Pages/')}`
    }
  }
})
