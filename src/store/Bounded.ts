import { createUserSlice, UserSlice } from './Slice/User.ts'
import { createToastSlice, ToastSlice } from './Slice/Toast.ts'
import { createWithEqualityFn } from 'zustand/traditional'

const useBoundStore = createWithEqualityFn<UserSlice & ToastSlice>()(
  (...a) => ({
    ...createUserSlice(...a),
    ...createToastSlice(...a)
  })
)
export default useBoundStore
