import { devtools } from 'zustand/middleware'
import { StateCreator } from 'zustand'
import { ToastData } from '../../ts/interfaces/Toast.ts'

export interface ToastSlice {
  severity: 'success' | 'info' | 'warn' | 'error'
  summary: string
  detail: string
  isDisplayed: boolean
  setToastData: (toastData: ToastData) => void
  setDisplayed: (display: boolean) => void
}

export const createToastSlice: StateCreator<
  ToastSlice,
  [],
  [['zustand/devtools', never]]
> = devtools((set) => ({
  severity: 'info',
  summary: 'Info',
  detail: 'Message',
  isDisplayed: false,
  setToastData: (toastData) => {
    set(() => ({
      severity: toastData.severity,
      summary: toastData.summary,
      detail: toastData.detail,
      isDisplayed: true
    }))
  },
  setDisplayed: (display) => {
    setTimeout(() => {
      set(() => ({ isDisplayed: display }))
    }, 500)
  }
}))
