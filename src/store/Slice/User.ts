import { devtools } from 'zustand/middleware'
import { StateCreator } from 'zustand'

export interface UserSlice {
  isAlreadyCreated: boolean
  isLogged: boolean
  setAlreadyCreated: (alreadyCreated: boolean) => void
  setLogged: (logged: boolean) => void
  userId: string
  setUserId: (userId: string) => void
}

export const createUserSlice: StateCreator<
  UserSlice,
  [],
  [['zustand/devtools', never]]
> = devtools((set) => ({
  isAlreadyCreated: false,
  isLogged: false,
  userId: '',
  setAlreadyCreated: (alreadyCreated: boolean) => {
    set(() => ({ isAlreadyCreated: alreadyCreated }))
  },
  setLogged: (logged: boolean) => {
    set(() => ({ isLogged: logged }))
  },
  setUserId: (userId: string) => {
    set(() => ({ userId: userId }))
  }
}))
