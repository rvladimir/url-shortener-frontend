import { createBrowserRouter } from 'react-router-dom'
import LoginPage from '../Pages/LoginPage/LoginPage.tsx'
import RegisterPage from '../Pages/RegisterPage/RegisterPage.tsx'
import DashboardPage from '../Pages/DashboardPage/DashboardPage.tsx'
import ProtectedRoute from '../components/ProtectedRoute/ProtectedRoute.tsx'
import ShortlinkPage from '../Pages/ShortlinkPage/ShortlinkPage.tsx'

const router = createBrowserRouter([
  {
    path: '/',
    element: (
      <ProtectedRoute>
        <LoginPage />
      </ProtectedRoute>
    )
  },
  {
    path: '/login',
    element: (
      <ProtectedRoute>
        <LoginPage />
      </ProtectedRoute>
    )
  },
  {
    path: '/register',
    element: (
      <ProtectedRoute>
        <RegisterPage />
      </ProtectedRoute>
    )
  },
  {
    path: '/dashboard',
    element: (
      <ProtectedRoute>
        <DashboardPage />
      </ProtectedRoute>
    ),
    children: [
      {
        index: true,
        element: (
          <>
            <h1>Dashboard</h1>
          </>
        )
      },
      {
        path: 'shortlinks',
        element: <ShortlinkPage />
      },
      {
        path: 'analytics',
        element: (
          <>
            <h1>Analytics</h1>
          </>
        )
      }
    ]
  }
])

export default router
