import { FC, ReactNode } from 'react'

interface FieldWrapperProps {
  children: ReactNode
  message: ReactNode
  icon?: ReactNode
}

const FieldWrapper: FC<FieldWrapperProps> = ({ children, message, icon }) => {
  return (
    <div className="field">
      <span
        className={icon ? 'p-float-label p-input-icon-right' : 'p-float-label'}
      >
        {icon}
        {children}
      </span>
      {message}
    </div>
  )
}

export default FieldWrapper
