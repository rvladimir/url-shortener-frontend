import { FC, ReactNode, useEffect, useRef } from 'react'
import { Toast } from 'primereact/toast'
import useBoundStore from '@store/Bounded.ts'

interface RootProps {
  children: ReactNode
}

const GlobalToast: FC<RootProps> = ({ children }) => {
  const { severity, summary, detail, isDisplayed } = useBoundStore(
    (state) => state
  )
  const toast = useRef<Toast>({} as Toast)

  const show = () => {
    if (isDisplayed) {
      toast.current.show({
        severity,
        summary,
        detail
      })
    }
  }

  useEffect(() => {
    show()
  }, [isDisplayed])

  return (
    <>
      <Toast ref={toast} />
      {children}
    </>
  )
}

export default GlobalToast
