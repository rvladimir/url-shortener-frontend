import { FC } from 'react'
import { MegaMenu } from 'primereact/megamenu'

interface ItemLabel {
  label: string
}

interface Item {
  label: string
  items: ItemLabel[]
}

interface MenuLabel {
  label: string
  icon: string
  items?: Item[][]
}

interface MenuProps {
  menuLabels: MenuLabel[]
}

const Menu: FC<MenuProps> = ({ menuLabels }) => {
  return (
    <div className="card">
      <MegaMenu
        model={menuLabels}
        orientation="vertical"
        style={{ borderRadius: '0.5rem', width: '100%' }}
      />
    </div>
  )
}

export default Menu
