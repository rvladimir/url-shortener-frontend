import { Container, Row } from 'react-grid-system'

function PageProgressSpinner() {
  return (
    <Container fluid>
      <Row className="full-h" align="center" justify="center">
        <PageProgressSpinner />
      </Row>
    </Container>
  )
}

export default PageProgressSpinner
