import { FC } from 'react'
import { Col, Row } from 'react-grid-system'
import { Steps } from 'primereact/steps'

import './StepperStyles.css'

const Stepper: FC = () => {
  return (
    <Row className="height-stepper" align="center" justify="center">
      <Col md={6}>
        <Steps
          model={[
            { label: 'Create User' },
            { label: 'Add Domain' },
            { label: 'Login' }
          ]}
          activeIndex={2}
        />
      </Col>
    </Row>
  )
}

export default Stepper
