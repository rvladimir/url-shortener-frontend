import { FC, ReactNode, useEffect } from 'react'
import useBoundStore from '@store/Bounded.ts'
import { Outlet, useNavigate } from 'react-router-dom'
import { useQuery } from 'react-query'
import UserService from '@/services/UserService.ts'
import { jwtDecode } from 'jwt-decode'
import { Col, Container, Row } from 'react-grid-system'
import { ProgressSpinner } from 'primereact/progressspinner'

interface UserData {
  userId: string
  userEmail: string
}

interface ProtectedRouteProps {
  children?: ReactNode
}

const ProtectedRoute: FC<ProtectedRouteProps> = ({ children }) => {
  const {
    isLogged: isUserLogged,
    isAlreadyCreated: isUserAlreadyCreated,
    setAlreadyCreated,
    setLogged,
    setUserId
  } = useBoundStore((state) => state)

  const navigate = useNavigate()

  const { isLoading } = useQuery(['checkUser'], UserService.userStatusCheck, {
    onSuccess: (data) => {
      setAlreadyCreated(data.created)
    }
  })

  useEffect(() => {
    const token = localStorage.getItem('token') || ''

    if (token !== '') {
      const jwtDecoded = jwtDecode(token)
      const userData = JSON.parse(jwtDecoded.sub!.toString()) as UserData
      const isExpiredToken = Date.now() > jwtDecoded.exp! * 1000

      setLogged(!isExpiredToken)
      setUserId(userData.userId)
    } else {
      setLogged(false)
    }

    if (!isLoading) {
      if (!isUserAlreadyCreated && !isUserLogged) {
        return navigate('/register')
      }

      if (isUserAlreadyCreated && !isUserLogged) {
        return navigate('/login')
      }

      if (isUserAlreadyCreated && isUserLogged) {
        return navigate('/dashboard')
      }
    }
  }, [isLoading])

  const spinner = (
    <Container fluid>
      <Row className="full-h" align="center" justify="center">
        <Col>
          <div className="card flex justify-content-center">
            <ProgressSpinner />
          </div>
          <h2 className="text-center">Wait a moment please ...</h2>
        </Col>
      </Row>
    </Container>
  )

  if (isLoading) {
    return spinner
  }

  return children ? children : <Outlet />
}

export default ProtectedRoute
