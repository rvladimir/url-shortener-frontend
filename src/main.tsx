import React from 'react'
import ReactDOM from 'react-dom/client'
import GlobalToast from './components/GlobalToast/GlobalToast.tsx'
import router from '@/routes/Routes.tsx'
import { RouterProvider } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'

import 'primereact/resources/themes/saga-blue/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'
import './index.css'

const queryClient = new QueryClient()

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <GlobalToast>
        <RouterProvider router={router} />
      </GlobalToast>
    </QueryClientProvider>
  </React.StrictMode>
)
