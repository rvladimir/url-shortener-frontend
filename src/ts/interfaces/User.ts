export interface UserStatusCheck {
  created: boolean
}

export interface UserLogged {
  token: string
}
