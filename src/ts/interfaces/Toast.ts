export interface ToastData {
  severity: 'success' | 'info' | 'warn' | 'error' | undefined
  summary: string
  detail: string
}
