import { Shortlink } from '../../Pages/ShortlinkPage/ShortlinkPage.tsx'

export interface UrlPage {
  content: Shortlink[]
  totalPages: number
  totalElements: number
  numberOfElements: number
  size: number
  number: 0
}
