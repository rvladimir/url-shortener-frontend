export interface Shortlink {
  id: number
  hash: string
  name: string
  link: string
  clicks: string
  creationDate: string
}
