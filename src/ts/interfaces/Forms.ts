export interface RegisterFormData {
  name: string
  lastName: string
  email: string
  password: string
}

export interface RegisterFormErrors {
  name?: string
  lastName?: string
  email?: string
  password?: string
}

export interface LoginFormData {
  email: string
  password: string
}

export interface LoginFormErrors {
  email?: string
  password?: string
}

export interface ShortlinkFormData {
  id?: number
  user?: User
  name: string
  link: string
  hash: string
}

export interface ShortlinkFormErrors {
  name?: string
  link?: string
  hash?: string
}

export interface User {
  id: number
}
