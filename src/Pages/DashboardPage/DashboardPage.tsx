import { FC } from 'react'
import { Col, Container, Row } from 'react-grid-system'
import { Divider } from 'primereact/divider'
import Menu from '@components/Menu/Menu.tsx'

import './DashboardPageStyles.css'
import { Outlet, useNavigate } from 'react-router-dom'

const DashboardPage: FC = () => {
  const navigate = useNavigate()

  const itemsMenu = [
    {
      label: 'Shortlinks',
      icon: 'pi pi-link',
      command: () => {
        return navigate('/dashboard/shortlinks')
      }
    },
    {
      label: 'Analytics',
      icon: 'pi pi-chart-bar',
      command: () => {
        return navigate('/dashboard/analytics')
      }
    }
  ]

  const itemsMenuSettings = [
    {
      label: 'Settings',
      icon: 'pi pi-cog'
    }
  ]

  return (
    <Container fluid>
      <Row className="full-h">
        <Col xs={3} md={2}>
          <Row align="center" justify="center">
            <span className="header-menu">
              <h3>URL - SHORTENER</h3>
            </span>
          </Row>
          <Menu menuLabels={itemsMenu} />
          <Divider layout="horizontal" />
          <Menu menuLabels={itemsMenuSettings} />
        </Col>
        <Col xs={9} md={10}>
          <Outlet />
        </Col>
      </Row>
    </Container>
  )
}

export default DashboardPage
