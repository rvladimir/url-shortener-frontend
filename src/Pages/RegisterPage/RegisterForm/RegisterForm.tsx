import { FC } from 'react'
import { FormikValues, useFormik } from 'formik'
import { Button } from 'primereact/button'
import { Divider } from 'primereact/divider'
import { InputText } from 'primereact/inputtext'
import { classNames } from 'primereact/utils'
import { Password } from 'primereact/password'
import { Col, Row } from 'react-grid-system'
import { Card } from 'primereact/card'
import { RegisterFormErrors, RegisterFormData } from '@ts/interfaces/Forms.ts'
import FieldWrapper from '@components/FieldWrapper/FieldWrapper.tsx'

import './RegisterFormStyles.css'

const RegisterForm: FC = () => {
  /*  const [formData, setFormData] = useState<FormData>({
      name: '',
      email: '',
      password: ''
    })*/

  const formik = useFormik({
    initialValues: {
      name: '',
      lastName: '',
      email: '',
      password: ''
    },
    validate: (formData: RegisterFormData) => {
      const errors: RegisterFormErrors = {}

      if (!formData.name) {
        errors.name = 'Name is required.'
      }

      if (!formData.lastName) {
        errors.lastName = 'Last Name is required.'
      }

      if (!formData.email) {
        errors.email = 'Email is required.'
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formData.email)
      ) {
        errors.email = 'Invalid email address. E.g. example@email.com'
      }

      if (!formData.password) {
        errors.password = 'Password is required.'
      }

      return errors
    },

    onSubmit: (_, { resetForm }) => {
      //setFormData(formData)
      //UserService.createUser(formData).then((response) => {
      //})
      resetForm()
    }
  })

  const isFormFieldValid = (field: string, formik: FormikValues) =>
    !!(formik.touched[field] && formik.errors[field])

  const getFormErrorMessage = (field: string, formik: FormikValues) => {
    return (
      isFormFieldValid(field, formik) && (
        <small className="p-error">{formik.errors[field]}</small>
      )
    )
  }

  const passwordHeader = <h6>Pick a password</h6>
  const passwordFooter = (
    <>
      <Divider />
      <p className="mt-2">Suggestions</p>
      <ul className="pl-2 ml-2 mt-0" style={{ lineHeight: '1.5' }}>
        <li>At least one lowercase</li>
        <li>At least one uppercase</li>
        <li>At least one numeric</li>
        <li>Minimum 8 characters</li>
      </ul>
    </>
  )

  const fieldName = (
    <FieldWrapper message={getFormErrorMessage('name', formik)}>
      <InputText
        id="name"
        name="name"
        value={formik.values.name}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('name', formik)
        })}
      />
      <label
        htmlFor="name"
        className={classNames({
          'p-error': isFormFieldValid('name', formik)
        })}
      >
        Name*
      </label>
    </FieldWrapper>
  )

  const fieldLastName = (
    <FieldWrapper message={getFormErrorMessage('lastName', formik)}>
      <InputText
        id="lastName"
        name="lastName"
        value={formik.values.lastName}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('lastName', formik)
        })}
      />
      <label
        htmlFor="lastName"
        className={classNames({
          'p-error': isFormFieldValid('lastName', formik)
        })}
      >
        Lastname*
      </label>
    </FieldWrapper>
  )

  const iconEmail = <i className="pi pi-envelope" />

  const fieldEmail = (
    <FieldWrapper
      message={getFormErrorMessage('email', formik)}
      icon={iconEmail}
    >
      <InputText
        id="email"
        name="email"
        value={formik.values.email}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('email', formik)
        })}
      />
      <label
        htmlFor="email"
        className={classNames({
          'p-error': isFormFieldValid('email', formik)
        })}
      >
        Email*
      </label>
    </FieldWrapper>
  )

  const fieldPassword = (
    <FieldWrapper message={getFormErrorMessage('password', formik)}>
      <Password
        id="password"
        name="password"
        value={formik.values.password}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        toggleMask
        className={classNames({
          'p-invalid': isFormFieldValid('password', formik)
        })}
        header={passwordHeader}
        footer={passwordFooter}
      />
      <label
        htmlFor="password"
        className={classNames({
          'p-error': isFormFieldValid('password', formik)
        })}
      >
        Password*
      </label>
    </FieldWrapper>
  )

  return (
    <Row align="center" justify="center">
      <Col md={4}>
        <h1 className="text-center">Url Shortener - Create User</h1>
        <Card>
          <form onSubmit={formik.handleSubmit} className="p-fluid">
            {fieldName}
            {fieldLastName}
            {fieldEmail}
            {fieldPassword}
            <div className="submitButton">
              <Button type="submit" label="Create" className="mt-2" />
            </div>
          </form>
        </Card>
      </Col>
    </Row>
  )
}

export default RegisterForm
