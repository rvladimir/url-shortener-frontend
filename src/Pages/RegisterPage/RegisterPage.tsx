import { FC } from 'react'
import { Container } from 'react-grid-system'
import Stepper from '@components/Stepper/Stepper.tsx'
import RegisterForm from '@pages/RegisterPage/RegisterForm/RegisterForm.tsx'

const RegisterPage: FC = () => {
  return (
    <Container fluid>
      <Stepper />
      <RegisterForm />
    </Container>
  )
}

export default RegisterPage
