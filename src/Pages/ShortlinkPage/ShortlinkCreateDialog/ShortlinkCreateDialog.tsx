import { Dispatch, FC, SetStateAction } from 'react'
import ShortlinkEditorDialog from '@pages/ShortlinkPage/components/ShortlinkEditorDialog/ShortlinkEditorDialog.tsx'
import { ShortlinkFormData } from '@ts/interfaces/Forms.ts'
import { useMutation } from 'react-query'
import ShortlinkService from '@services/ShortlinkService.ts'
import useBoundStore from '@store/Bounded.ts'
import { AxiosError } from 'axios'

interface ShortlinkCreateDialogProps {
  isVisible: boolean
  buttonLabel: 'Create' | 'Update'
  onClose: Dispatch<SetStateAction<boolean>>
  values: ShortlinkFormData
}

const ShortlinkCreateDialog: FC<ShortlinkCreateDialogProps> = ({
  isVisible,
  onClose,
  values
}) => {
  const { setToastData, setDisplayed } = useBoundStore((state) => state)

  const createMutation = useMutation({
    mutationFn: (shortlinkFormData: ShortlinkFormData) => {
      try {
        return ShortlinkService.createShortlink(shortlinkFormData)
      } catch (error) {
        throw error as AxiosError
      }
    },
    onError: (error: Error) => {
      onClose(false)
      const errorString = error.message.toString()
      setToastData({
        detail: errorString,
        severity: 'warn',
        summary: 'Error'
      })
      setDisplayed(false)
    },
    onSuccess: () => {
      onClose(false)
      setToastData({
        detail: 'Shortlink created succesful.',
        severity: 'info',
        summary: 'Created!'
      })
      setDisplayed(false)
    }
  })

  const handleSubmit = (shortlinkFormData: ShortlinkFormData) => {
    createMutation.mutate(shortlinkFormData)
  }

  return (
    <ShortlinkEditorDialog
      isVisible={isVisible}
      buttonLabel="Create"
      onClose={onClose}
      values={values}
      handleSubmit={handleSubmit}
      title="Create new Shortlink"
    />
  )
}

export default ShortlinkCreateDialog
