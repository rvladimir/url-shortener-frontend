import { FC, useState } from 'react'
import ShortlinkTable from '@pages/ShortlinkPage/ShortlinkTable/ShortlinkTable.tsx'
import { Button } from 'primereact/button'
import { Col, Container, Row } from 'react-grid-system'
import ShortlinkCreateDialog from '@pages/ShortlinkPage/ShortlinkCreateDialog/ShortlinkCreateDialog.tsx'

import './ShortlinkPageStyles.css'
import ShortlinkEditDialog from '@pages/ShortlinkPage/ShortlinkEditDialog/ShortlinkEditDialog.tsx'
import { Shortlink } from '@ts/interfaces/Shortlink.ts'
import useBoundStore from '@store/Bounded.ts'

const ShortlinkPage: FC = () => {
  const [showCreateDialog, setShowCreateDialog] = useState(false)
  const [showEditDialog, setShowEditDialog] = useState(false)
  const [editShortlinkData, setEditShortlinkData] = useState<Shortlink>()

  const { userId } = useBoundStore((state) => state)

  const emptyValues = {
    name: '',
    link: '',
    hash: '',
    user: {
      id: userId
    }
  }

  const onEditRow = (shortlink: Shortlink) => {
    setShowEditDialog(true)
    setEditShortlinkData(shortlink)
  }

  const createUrlButton = (
    <Button
      label="Create"
      icon="pi pi-plus"
      iconPos="right"
      raised
      onClick={() => setShowCreateDialog(true)}
    />
  )

  return (
    <>
      <ShortlinkCreateDialog
        isVisible={showCreateDialog}
        onClose={setShowCreateDialog}
        buttonLabel="Create"
        values={emptyValues}
      />
      <ShortlinkEditDialog
        isVisible={showEditDialog}
        buttonLabel="Update"
        onClose={setShowEditDialog}
        values={editShortlinkData || emptyValues}
      />
      <Container fluid>
        <Row>
          <Col className="col-header">
            <h1>Shortlinks</h1>
            {createUrlButton}
          </Col>
        </Row>
        <ShortlinkTable onEditRow={onEditRow} />
      </Container>
    </>
  )
}

export default ShortlinkPage
