import { FC, useEffect, useState } from 'react'
import {
  INITIAL_NUMBER_OF_ROWS,
  OPTION_NUMBER_OF_ROWS
} from '@pages/ShortlinkPage/constants/Constants.ts'
import { UrlPage } from '@ts/interfaces/Url.ts'
import { useQuery } from 'react-query'
import UrlService from '@services/ShortlinkService.ts'
import { Shortlink } from '@ts/interfaces/Shortlink.ts'
import Options from '@pages/ShortlinkPage/components/Options/Options.tsx'
import { Paginator, PaginatorPageChangeEvent } from 'primereact/paginator'
import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { Row } from 'react-grid-system'

interface ShortlinkTableProps {
  onEditRow: (shortlink: Shortlink) => void
}

const ShortlinkTable: FC<ShortlinkTableProps> = ({ onEditRow }) => {
  const [columnId, setColumnId] = useState()
  const [first, setFirst] = useState(0)
  const [totalElements, setTotalElements] = useState(0)
  const [page, setPage] = useState(0)
  const [numberOfRows, setNumberOfRows] = useState(INITIAL_NUMBER_OF_ROWS)

  const [dataTable, setDataTable] = useState<UrlPage>()

  const { refetch, isFetching } = useQuery(
    ['getUrls'],
    () => UrlService.getShortlinksPaginated(page, numberOfRows),
    {
      onSuccess: (data) => {
        setDataTable(data)
        setTotalElements(data.totalElements)
      }
    }
  )

  useEffect(() => {
    refetch()
  }, [numberOfRows, page])

  const editBodyTemplate = (value: Shortlink) => {
    if (columnId === value.id) {
      return <Options shortlink={value} onEdit={onEditRow} />
    }
    const creationDate = new Date(value.creationDate).toDateString()
    return <>{creationDate}</>
  }

  const onPageChange = async (event: PaginatorPageChangeEvent) => {
    setNumberOfRows(event.rows)
    setFirst(event.first)
    setPage(event.first / event.rows)
  }

  const shortlinkBodyTemplate = (value: Shortlink) => {
    return (
      <div style={{ paddingTop: '0px' }}>
        <h3>{value.name}</h3>
        <h4>{value.hash}</h4>
        <h4>{value.link}</h4>
      </div>
    )
  }

  return (
    <>
      <Row style={{ paddingTop: '10px' }}>
        <DataTable
          value={dataTable?.content}
          selectionMode="single"
          showHeaders={false}
          size="small"
          loading={isFetching}
          onRowMouseEnter={(e) => {
            setColumnId(e.data.id)
          }}
          onRowMouseLeave={() => {
            setColumnId(undefined)
          }}
          tableStyle={{
            minWidth: '80vw',
            maxWidth: '100vw'
          }}
        >
          <Column
            field="shortlink"
            body={shortlinkBodyTemplate}
            style={{ width: '30vw' }}
          />
          <Column field="clicks" align="center" style={{ width: '10vw' }} />
          <Column field="creationDate" align="center" body={editBodyTemplate} />
        </DataTable>
      </Row>

      <Row align="center" justify="center">
        <Paginator
          first={first}
          rows={numberOfRows}
          totalRecords={totalElements}
          rowsPerPageOptions={OPTION_NUMBER_OF_ROWS}
          onPageChange={onPageChange}
          style={{ paddingTop: '5vh' }}
        />
      </Row>
    </>
  )
}

export default ShortlinkTable
