import { Dispatch, FC, SetStateAction } from 'react'
import ShortlinkEditorDialog from '@pages/ShortlinkPage/components/ShortlinkEditorDialog/ShortlinkEditorDialog.tsx'
import { ShortlinkFormData } from '@ts/interfaces/Forms.ts'

interface ShortlinkEditDialogProps {
  isVisible: boolean
  buttonLabel: 'Create' | 'Update'
  onClose: Dispatch<SetStateAction<boolean>>
  values: ShortlinkFormData
}

const ShortlinkEditDialog: FC<ShortlinkEditDialogProps> = ({
  isVisible,
  onClose,
  values
}) => {
  const handleSubmit = () => {}

  return (
    <ShortlinkEditorDialog
      isVisible={isVisible}
      buttonLabel="Create"
      onClose={onClose}
      values={values}
      handleSubmit={handleSubmit}
      title="Edit Shortlink"
    />
  )
}

export default ShortlinkEditDialog
