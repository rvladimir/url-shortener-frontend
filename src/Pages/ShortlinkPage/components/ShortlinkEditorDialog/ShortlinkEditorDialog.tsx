import { FC } from 'react'
import { Dialog } from 'primereact/dialog'
import { Container, Row } from 'react-grid-system'
import ShortlinkForm from '@pages/ShortlinkPage/components/ShortlinkForm/ShortlinkForm.tsx'
import { ShortlinkFormData } from '@ts/interfaces/Forms.ts'

import './ShortlinkEditorDialogStyles.css'

interface ShortlinkEditorDialogProps {
  isVisible: boolean
  buttonLabel: 'Create' | 'Update'
  onClose: (isVisible: boolean) => void
  values: ShortlinkFormData
  title: string
  handleSubmit: (shortlinkFormData: ShortlinkFormData) => void
}

const ShortlinkEditorDialog: FC<ShortlinkEditorDialogProps> = ({
  isVisible,
  buttonLabel,
  onClose,
  values,
  title,
  handleSubmit
}) => {
  return (
    <Dialog
      header={title}
      onHide={() => onClose(false)}
      visible={isVisible}
      className="dialog"
      draggable={false}
    >
      <Container>
        <Row>
          <ShortlinkForm
            handleSubmit={handleSubmit}
            values={values}
            buttonLabel={buttonLabel}
            onClose={onClose}
          />
        </Row>
      </Container>
    </Dialog>
  )
}

export default ShortlinkEditorDialog
