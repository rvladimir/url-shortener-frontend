import { FC } from 'react'
import { FormikValues, useFormik } from 'formik'
import { ShortlinkFormData, ShortlinkFormErrors } from '@ts/interfaces/Forms.ts'
import { Card } from 'primereact/card'
import FieldWrapper from '@components/FieldWrapper/FieldWrapper.tsx'
import { InputText } from 'primereact/inputtext'
import { classNames } from 'primereact/utils'
import { Col, Row } from 'react-grid-system'
import { Button } from 'primereact/button'
import { faker } from '@faker-js/faker'

import './ShortlinkFormStyles.css'

interface ShortlinkFormProps {
  handleSubmit: (shortlinkForm: ShortlinkFormData) => void
  values: ShortlinkFormData
  buttonLabel: 'Create' | 'Update'
  onClose: (isVisible: boolean) => void
}

const ShortlinkForm: FC<ShortlinkFormProps> = ({
  handleSubmit,
  values,
  buttonLabel,
  onClose
}) => {
  const formik = useFormik({
    initialValues: values,

    validate: (shortlinkFormData: ShortlinkFormData) => {
      const shortlinkFormErrors: ShortlinkFormErrors = {}

      if (!shortlinkFormData.name) {
        shortlinkFormErrors.name = 'Name is required.'
      }

      if (!shortlinkFormData.link) {
        shortlinkFormErrors.link = 'Link is required.'
      } else if (
        !/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)$/i.test(
          shortlinkFormData.link
        )
      ) {
        shortlinkFormErrors.link =
          'Link should be a valid URL. e.g.: http(s)://www.example.com/a'
      }

      if (!shortlinkFormData.hash) {
        shortlinkFormErrors.hash = 'Hash is required.'
      }

      return shortlinkFormErrors
    },

    onSubmit: (shortlinkFormData: ShortlinkFormData, { resetForm }) => {
      handleSubmit(shortlinkFormData)
      resetForm()
    }
  })

  const isFormFieldValid = (field: string, formik: FormikValues) =>
    !!(formik.touched[field] && formik.errors[field])

  const getFormErrorMessage = (field: string, formik: FormikValues) => {
    return (
      isFormFieldValid(field, formik) && (
        <small className="p-error">{formik.errors[field]}</small>
      )
    )
  }

  const onGenerateHash = () => {
    formik.setFieldValue(
      'hash',
      faker.string.alphanumeric({
        length: 6,
        casing: 'upper'
      })
    )
  }

  const fieldName = (
    <FieldWrapper message={getFormErrorMessage('name', formik)}>
      <InputText
        id="name"
        name="name"
        value={formik.values.name}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('name', formik)
        })}
      />
      <label
        htmlFor="name"
        className={classNames({
          'p-error': isFormFieldValid('name', formik)
        })}
      >
        Name*
      </label>
    </FieldWrapper>
  )

  const fieldLink = (
    <FieldWrapper message={getFormErrorMessage('link', formik)}>
      <InputText
        id="link"
        name="link"
        value={formik.values.link}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('link', formik)
        })}
      />
      <label
        htmlFor="link"
        className={classNames({
          'p-error': isFormFieldValid('link', formik)
        })}
      >
        Link*
      </label>
    </FieldWrapper>
  )

  const fieldHash = (
    <FieldWrapper message={getFormErrorMessage('hash', formik)}>
      <InputText
        id="hash"
        name="hash"
        value={formik.values.hash}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('hash', formik)
        })}
      />
      <label
        htmlFor="hash"
        className={classNames({
          'p-error': isFormFieldValid('hash', formik)
        })}
      >
        Hash*
      </label>
    </FieldWrapper>
  )

  return (
    <Card className="card-style">
      <form onSubmit={formik.handleSubmit}>
        <Row align="center" justify="center" className="p-fluid">
          <Col md={12}>
            {fieldName}
            {fieldLink}
            <Row align="center" justify="center">
              <Col md={8}>{fieldHash}</Col>
              <Col md={4}>
                <Button
                  severity="secondary"
                  label="Generate Hash"
                  onClick={onGenerateHash}
                  type="button"
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col className="col-bottom">
            <Button
              label="Cancel"
              severity="warning"
              type="button"
              onClick={() => onClose(false)}
            />
            <Button
              className="button-submit"
              type="submit"
              label={buttonLabel}
            />
          </Col>
        </Row>
      </form>
    </Card>
  )
}

export default ShortlinkForm
