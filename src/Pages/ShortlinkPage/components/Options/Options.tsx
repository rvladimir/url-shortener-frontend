import { FC } from 'react'
import { Button } from 'primereact/button'
import useBoundStore from '@store/Bounded.ts'
import { Shortlink } from '@ts/interfaces/Shortlink.ts'

interface OptionsProps {
  shortlink: Shortlink
  onEdit: (shortlink: Shortlink) => void
}

const Options: FC<OptionsProps> = ({ shortlink, onEdit }) => {
  const { setToastData, setDisplayed } = useBoundStore((state) => state)

  const onClickVisit = (url: string) => {
    window.open(url, '_blank', 'noopener,noreferrer')
  }

  const onClickCopy = (shortlink: string) => {
    navigator.clipboard.writeText('www.s.vladimirv.com/'.concat(shortlink))
    setToastData({
      detail: 'Shortlink copied to Clipboard',
      severity: 'success',
      summary: 'Success'
    })
    setDisplayed(false)
  }

  const onClickEdit = (shortlink: Shortlink) => {
    onEdit(shortlink)
  }

  return (
    <>
      <Button
        tooltip="Visit"
        tooltipOptions={{ position: 'top' }}
        icon="pi pi-external-link"
        rounded
        text
        severity="success"
        size="large"
        aria-label="Visit"
        onClick={() => onClickVisit(shortlink.link)}
      />
      <Button
        tooltip="Copy"
        tooltipOptions={{ position: 'top' }}
        icon="pi pi-copy"
        rounded
        text
        size="large"
        aria-label="Copy"
        onClick={() => onClickCopy(shortlink.hash)}
      />
      <Button
        tooltip="Edit"
        tooltipOptions={{ position: 'top' }}
        icon="pi pi-pencil"
        rounded
        text
        size="large"
        severity="warning"
        aria-label="Edit"
        onClick={() => onClickEdit(shortlink)}
      />
      <Button
        tooltip="Delete"
        tooltipOptions={{ position: 'top' }}
        icon="pi pi-trash"
        rounded
        text
        severity="danger"
        size="large"
        aria-label="Delete"
      />
    </>
  )
}

export default Options
