import { FC } from 'react'
import { FormikValues, useFormik } from 'formik'
import { Button } from 'primereact/button'
import { InputText } from 'primereact/inputtext'
import { classNames } from 'primereact/utils'
import { Password } from 'primereact/password'
import { Col, Row } from 'react-grid-system'
import { Card } from 'primereact/card'
import { LoginFormErrors, LoginFormData } from '@ts/interfaces/Forms.ts'
import FieldWrapper from '@components/FieldWrapper/FieldWrapper.tsx'

interface LoginFormProps {
  handleSubmit: (formLogin: LoginFormData) => void
}

const LoginForm: FC<LoginFormProps> = ({ handleSubmit }) => {
  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },

    validate: (loginFormData: LoginFormData) => {
      const loginFormErrors: LoginFormErrors = {}

      if (!loginFormData.email) {
        loginFormErrors.email = 'Email is required.'
      } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(loginFormData.email)
      ) {
        loginFormErrors.email = 'Invalid email address. E.g. example@email.com'
      }

      if (!loginFormData.password) {
        loginFormErrors.password = 'Password is required.'
      }

      return loginFormErrors
    },

    onSubmit: (loginForm: LoginFormData, { resetForm }) => {
      handleSubmit(loginForm)
      resetForm()
    }
  })

  const isFormFieldValid = (field: string, formik: FormikValues) =>
    !!(formik.touched[field] && formik.errors[field])

  const getFormErrorMessage = (field: string, formik: FormikValues) => {
    return (
      isFormFieldValid(field, formik) && (
        <small className="p-error">{formik.errors[field]}</small>
      )
    )
  }

  const iconEmail = <i className="pi pi-envelope" />

  const fieldEmail = (
    <FieldWrapper
      message={getFormErrorMessage('email', formik)}
      icon={iconEmail}
    >
      <InputText
        id="email"
        name="email"
        value={formik.values.email}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        className={classNames({
          'p-invalid': isFormFieldValid('email', formik)
        })}
      />
      <label
        htmlFor="email"
        className={classNames({
          'p-error': isFormFieldValid('email', formik)
        })}
      >
        Email*
      </label>
    </FieldWrapper>
  )

  const fieldPassword = (
    <FieldWrapper message={getFormErrorMessage('password', formik)}>
      <Password
        id="password"
        name="password"
        value={formik.values.password}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        feedback={false}
        className={classNames({
          'p-invalid': isFormFieldValid('password', formik)
        })}
      />
      <label
        htmlFor="password"
        className={classNames({
          'p-error': isFormFieldValid('password', formik)
        })}
      >
        Password*
      </label>
    </FieldWrapper>
  )

  return (
    <Row className="full-h" align="center" justify="center">
      <Col md={4}>
        <h1 className="text-center">Url Shortener - Login User</h1>
        <Card>
          <form onSubmit={formik.handleSubmit} className="p-fluid">
            {fieldEmail}
            {fieldPassword}
            <div>
              <Button type="submit" label="Login" className="mt-2" />
            </div>
          </form>
        </Card>
      </Col>
    </Row>
  )
}

export default LoginForm
