import { FC } from 'react'
import { Container } from 'react-grid-system'
import LoginForm from './LoginForm/LoginForm.tsx'
import UserService from '@services/UserService.ts'
import { LoginFormData } from '@ts/interfaces/Forms.ts'
import { AxiosError } from 'axios'
import useBoundStore from '@store/Bounded.ts'
import { useNavigate } from 'react-router-dom'

const LoginPage: FC = () => {
  const navigate = useNavigate()
  const { setToastData, setDisplayed } = useBoundStore((state) => state)

  const handleSubmit = async (formLogin: LoginFormData) => {
    try {
      const userLogged = await UserService.loginUser(formLogin)
      localStorage.setItem('token', userLogged.token)
      navigate('/dashboard')
    } catch (error) {
      const axiosError = error as AxiosError
      const detail = (axiosError.response?.data as string) ?? axiosError.message
      setToastData({
        detail,
        severity: 'warn',
        summary: 'Error'
      })
      setDisplayed(false)
    }
  }

  return (
    <Container fluid>
      <LoginForm handleSubmit={handleSubmit} />
    </Container>
  )
}

export default LoginPage
