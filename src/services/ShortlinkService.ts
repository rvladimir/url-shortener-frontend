import Api from './Api.ts'
import { UrlPage } from '@ts/interfaces/Url.ts'
import { ShortlinkFormData } from '@ts/interfaces/Forms.ts'

export default {
  async getShortlinksPaginated(page: number, size: number): Promise<UrlPage> {
    const shortlinkPage = await Api().get(
      `api/url/urls?page=${page}&size=${size}`
    )
    return shortlinkPage.data
  },

  async createShortlink(shortlinkFormData: ShortlinkFormData) {
    await Api().post('api/url/create', shortlinkFormData)
  }
}
