import Api from './Api'
import { LoginFormData } from '@ts/interfaces/Forms.ts'
import { UserLogged, UserStatusCheck } from '@ts/interfaces/User.ts'

export default {
  async userStatusCheck(): Promise<UserStatusCheck> {
    const response = await Api().post('api/user/check')
    return response.data
  },

  async loginUser(formLogin: LoginFormData): Promise<UserLogged> {
    const userLogged = await Api().post('api/user/login', formLogin)
    return userLogged.data
  }
}
